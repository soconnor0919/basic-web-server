#### Basic Docker Web Server

This repository includes all the files required to run a basic live development web server through docker.

### Referenced commands:

Installing brew:
```sh 
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

Installing the necessary packages:
```sh
brew install git colima visual-studio-code docker docker-compose
```

Starting the environment:
```sh
colima start
docker-compose up -d
```